# Use Gitlab CI to build markdown documents with pandoc-latex-build

## Instructions

### Clone this repository

You can replace the default template with one you prefer.

**Alternatively**, set up a pandoc-latex-build project 
(for this you need
[pandoc-latex-build](https://codebase.helmholtz.cloud/hmc/hmc-fair-data-commons/pandoc-latex-build)
installed on your computer)
and add the `.gitlab-ci.yml` file provided in this repository.

**Optional:** Adapt the runner you want to use, if you prefer 
not using the default runners provided by this Gitlab instance.

## Usage

Once you have set up the `.gitlab-ci.yml` file and push a commit that includes
the file,
the runner should start automatically and be executed on every new commit to build the
document.

The resulting document should be available as [Gitlab Pages](../../../pages).

But of course the documents are available as 
[job artifacts](https://docs.gitlab.com/ee/ci/pipelines/job_artifacts.html#access-the-latest-job-artifacts-by-url) 
as well and you can link them using Badges 
(['Settings' -> 'General'](../../../edit) expand 'Badges').

Further you can use [pre-commit](https://pre-commit.com/) in
continuous integration to check your source code files (markdown, tex, ...).
Look at the provided [pre-commit hooks](https://pre-commit.com/hooks.html) to
see possible tools.

This is already done as an example in [.gitlab-ci.yml](.gitlab-ci.yml) in this
repository. The jobs are running in the same stage in parallel. But
you could also run first the check and then the deployment of the pages
in sequence.
Since the markdown files in this repository do not fulfill these requirements,
the CI job is allowed to fail. You can see the errors/hints as an example.
Since pandoc is still able to handle the files, we can go with them.
